#!/usr/bin/env bash
source /home/yohlane/twitterCleaner/config.sh

usage() {
    cat <<EOT
Usages:
  $0 [login] [-t] [-l] [-s "x days/months ago"]
  login : Used to login to twitter API (first use)
  -t : delete twittes
  -l : delete likes
  -s : defined before when to delete (unix date --date format) 
    default "1 month ago"
    ex:
      "1 month ago"
      "3 months ago"
      "1 day ago"
EOT
}

login(){
  twurl authorize --consumer-key $key --consumer-secret $secret
}

deleteStatus(){
  echo "cleaning post $1">&2
  twurl account $account -X POST "/1.1/statuses/destroy/$1.json" >/dev/null
}

deleteLike(){
  echo "cleaning like $1">&2
  twurl account $account -X POST "/1.1/favorites/destroy.json?id=$1" #>/dev/null
}

t=0
l=0

while getopts "tlhs:" opt; do
  case $opt in
    h)
      usage
      exit
      ;;
    t)
      t=1
      ;;
    l)
      l=1
      ;;
    s)
      s="${OPTARG}"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      exit
      ;;
  esac
done

if [ "$#" -eq 1 ] && [ "$1" == "login" ]; then
    login
fi

if [ $t == 0 ] && [ $l == 0 ]; then
  t=1
  l=1
fi

if [ -z "$s" ]; then
  to_check_tweets=$(date --date="$tweets_s" +%s)
  to_check_likes=$(date --date="$likes_s" +%s)
else
  to_check_tweets=$(date --date="$s" +%s)
  to_check_likes=$to_check_tweets
fi

tweet_to_delete=1
like_to_delete=1

if [ $t == 1 ]; then
  echo "----------------------------"
  echo "Cleaning Tweets"
  echo "----------------------------"
  while [ $tweet_to_delete -ge 1 ] 
  do
    tweet_to_delete=0
    tweet_to_delete=$(twurl account $account "/1.1/statuses/user_timeline.json?screen_name=$account&count=200" | 
      jq '.[] | select( .created_at | strptime("%a %b %d %H:%M:%S +0000 %Y")|mktime<='$to_check_tweets') | {id_str: .id_str}' | 
      grep id_str |
      awk -F'\"' '{print $4}' | while read line ; do deleteStatus $line ; echo 1; done | wc -l)
  done
fi

if [ $l == 1 ]; then
  echo "----------------------------"
  echo "Cleaning Likes"
  echo "----------------------------"
  while [ $like_to_delete -ge 1 ]
  do
    like_to_delete=0
    like_to_delete=$(twurl account $account "/1.1/favorites/list.json?screen_name=$account&count=200" | 
      jq '.[] | select( .created_at | strptime("%a %b %d %H:%M:%S +0000 %Y")|mktime<='$to_check_likes') | {id_str: .id_str}' | 
      grep id_str |
      awk -F'\"' '{print $4}' | while read line ; do deleteLike $line ; echo 1; done | wc -l)
  done
fi

## Twitter cleaner
Small twitter cleaner for old posts and likes

Add app key and secret to config.sh if you want to do the twurl login

Add your account name in config.sh

```
Usages:
  ./twitter_cleaner.sh [login] [-t] [-l] [-s "x days/months ago"]
  login : Used to login to twitter API (first use)
  -t : delete twittes
  -l : delete likes
  -s : defined before when to delete (unix date --date format) 
    default "1 month ago"
    ex:
      "1 month ago"
      "3 months ago"
      "1 day ago"

```

## Requirements

* Twitter application : https://apps.twitter.com/
* twurl : https://github.com/twitter/twurl
* jq : https://stedolan.github.io/jq/

## FAQ
> It's not working ! Nothing is deleted !

The default free API limits the posts fetched at 200. If you want to delete old posts and have twitted more than 200 posts since the date choosed, the API will not show the older tweets.
It happens a lot with likes.

To fix, buy better API access or move the date closer.

## Author
@yohlane

 **Free Software, Hell Yeah!**
